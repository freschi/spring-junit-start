package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.omg.PortableInterceptor.SUCCESSFUL;
import org.springframework.format.datetime.joda.DateTimeParser;
import org.springframework.format.datetime.joda.LocalDateParser;

import at.spenger.junit.domain.Person.Sex;

public class ClubTest {
	private static DateTimeFormatter parser = DateTimeFormatter.ofPattern("dd-MM-yyyy").withZone(ZoneId.of("UTC"));

	@Test
	public void testEnter() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.getPersons().size() >= 1; 
	}

	@Test
	public void testNumberOf() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.numberOf() == 1;
	}
	
	@Test
	public void testAverage() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		assert club.averageAge() == 44;
	}
	
	@Test
	public void testSort() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		club.sort();
		assert club.getPersons().get(0).getLastName().equals("Aond");
		assert club.getPersons().get(1).getLastName().equals("Bond");
	}
	
	@Test
	public void testRemove() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		assertEquals("2 Persons in club", 2, club.numberOf());
		club.leave(0);
		assert club.numberOf() == 1;
	}
	
	@Test
	public void testDissolve() {
		Club club = new Club();
		Person person = new Person("James", "Bond", parser.parse("01-01-1970", LocalDate::from), Sex.MALE);
		club.enter(person);
		club.enter(new Person("James", "Aond", parser.parse("02-01-1970", LocalDate::from), Sex.MALE));
		assertEquals("2 Persons in club", 2, club.numberOf());
		club.dissolve();
		assert club.numberOf() == 0;
	}
	
	@Test
	public void testName() {
		Club club = new Club();
		club.setName("Hello World");
		assert club.getName().equals("Hello World");
		
	}

}
