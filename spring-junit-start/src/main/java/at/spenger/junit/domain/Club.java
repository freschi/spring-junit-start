package at.spenger.junit.domain;

import java.time.temporal.TemporalAmount;
import java.time.Duration;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.time.LocalDate;

import org.springframework.format.datetime.joda.LocalDateParser;

public class Club {
	private List<Person> l;
	private String name;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public double averageAge() {
		double total = 0;
		for(Person t : l) {
			Period period = Period.between(LocalDate.now(), t.getBirthday());
			total += period.getYears();
		}
		return total / numberOf();
	}
	
	public void sort() {
		l.sort(new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				int val;
				return (val = o1.getLastName().compareTo(o1.getLastName())) == 0 ? (o1.getFirstName().compareTo(o2.getFirstName())) : val;
			}
		});
	}
	
	public void leave(int index) {
		l.remove(index);
	}
	
	public void dissolve() {
		l.clear();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
